package com.doppler.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.doppler.services.TimerService;

import static com.doppler.services.JobSchedulerService.isJobServiceOn;

/**
 * Created by fabio on 24/01/2016.
 */
public class TimerSensorRestarter extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TimerSensorRestarter.class.getSimpleName(), "OnDestroy Broadcast Received");
        Log.i(TimerSensorRestarter.class.getSimpleName(), "Timer Service Stops!");

        if (isJobServiceOn(context)) {
            Log.i(TimerSensorRestarter.class.getSimpleName(), "Restarting Timer");
            context.startService(new Intent(context, TimerService.class));
        }

    }

}
