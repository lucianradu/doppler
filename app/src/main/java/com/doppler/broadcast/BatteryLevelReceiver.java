package com.doppler.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class BatteryLevelReceiver extends BroadcastReceiver {

    public static final String TAG = "[Power Manager]";

    @Override
    public void onReceive(Context context, final Intent intent) {
        Log.w(TAG, "Battery Level broadcast received...");
    }


}
