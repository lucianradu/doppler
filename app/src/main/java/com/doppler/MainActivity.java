package com.doppler;

import android.animation.ObjectAnimator;
import android.app.ActivityManager;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.doppler.broadcast.DeviceIdleReceiver;
import com.doppler.broadcast.NetworkChangeReceiver;
import com.doppler.database.DatabaseController;
import com.doppler.network.NetworkStatus;
import com.doppler.notification.NotificationType;
import com.doppler.services.JobSchedulerService;
import com.doppler.services.TimerService;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import me.toptas.fancyshowcase.FancyShowCaseView;
import pl.bclogic.pulsator4droid.library.PulsatorLayout;

import static com.doppler.notification.NotificationManager.sendNotification;
import static com.doppler.notification.NotificationType.INTERNET_DEFAULT;
import static com.doppler.notification.NotificationType.INTERNET_DOWN;
import static com.doppler.notification.NotificationType.INTERNET_UP;
import static com.doppler.services.JobSchedulerService.PING_DNS_JOB_ID;
import static com.doppler.services.JobSchedulerService.isJobServiceOn;
import static com.doppler.utils.SystemUtils.setSystemBarTheme;

public class MainActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

    private static final String TAG = "MainActivity";
    private static final String PREF_USER_FIRST_TIME = "user_first_time";

    public  Intent mServiceIntent;
    private Context context;
    private BroadcastReceiver mNetworkReceiver, mTimerReceiver;
    private DeviceIdleReceiver mDeviceIdleReceiver;
    private TimerService mTimerService;
    private PulsatorLayout mPulsator;
    private ImageView connectionIcon, networkIcon, menuIcon;
    private SwitchCompat trigger;
    private TextView radarStatus, connectionStatus, nextCheck;
    private DatabaseController databaseController;
    private ProgressBar mProgressBar;
    private SharedPreferences sharedPreferences;
    private android.app.NotificationManager notificationManager;

    private int timerInterval;
    private AdView mAdView;

    public int getTimerInterval() {
        return timerInterval;
    }

    public void setTimerInterval(int timerInterval) {
        this.timerInterval = timerInterval;
    }

    public Context getContext() {
        return context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "ON CREATE");
        setContentView(R.layout.activity_main);
        setSystemBarTheme(MainActivity.this, false);
        context = this;

        PreferenceManager.setDefaultValues(this, R.xml.pref_general, true);
        PreferenceManager.setDefaultValues(this, R.xml.pref_notification, true);

        getSupportActionBar().hide();
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        checkAppFirstRun();

        notificationManager = (android.app.NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);


        NetworkStatus networkStats = new NetworkStatus();
        databaseController = new DatabaseController(this);

        networkIcon = (ImageView) findViewById(R.id.networkIcon);

        connectionIcon = (ImageView) findViewById(R.id.connectionIcon);
        connectionIcon.setImageResource(R.drawable.ic_satellite);

        connectionStatus = (TextView) findViewById(R.id.connectionStatus);
        nextCheck = (TextView) findViewById(R.id.nextCheck);

        getNetworkIcon(networkStats.getNetworkConnectionType(this));

        mPulsator = (PulsatorLayout) findViewById(R.id.pulsator);

        // start pulsator
        mPulsator.start();

        if (android.os.Build.VERSION.SDK_INT >= 26){
            mPulsator.setDuration(12000);
        } else{
            mPulsator.setDuration(6000);
        }

        mPulsator.setCount(2); //debug line

        mTimerService = new TimerService(getApplicationContext());
        mServiceIntent = new Intent(getApplicationContext(), mTimerService.getClass());
//        stopService(mServiceIntent);

        radarStatus = (TextView) findViewById(R.id.radarStatus);
        radarStatus.setTextColor(getResources().getColor(R.color.colorGray));
        radarStatus.setText("INTERNET RADAR OFFLINE");

        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);

        trigger = (SwitchCompat) findViewById(R.id.trigger);
        trigger.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Log.d(TAG, "ON-CREATE: PORNESC TIMERUL");
                    showCountdown();

                    if (!isMyServiceRunning(mTimerService.getClass())) {
                        startService(mServiceIntent);
                    }

                    if (!isJobServiceOn(getContext())) {
                        scheduleJob();
                    }

                    mPulsator.start();
                    mPulsator.setCount(5);
                    mPulsator.setColor(INTERNET_DEFAULT.getNotificationColor());
                    radarStatus.setText("CHECKING CONNECTION...");
                    //TODO WAIT FOR NET TO BE AVAILABLE - APP MAY FREEZE IF NO NET AVAILABLE
                    if (databaseController.listLastNotification().equals("INTERNET AVAILABLE")) {
                        connectionIcon.setImageResource(INTERNET_UP.getNotificationIcon());
                        radarStatus.setTextColor(INTERNET_UP.getNotificationColor());
                        radarStatus.setText(databaseController.listLastNotification());
                        sendNotification(getContext(), INTERNET_UP, 1);
                    } else if (databaseController.listLastNotification().equals("INTERNET NOT AVAILABLE")) {
                        connectionIcon.setImageResource(R.drawable.ic_internet_connected_red);
                        radarStatus.setTextColor(INTERNET_DOWN.getNotificationColor());
                        radarStatus.setText(databaseController.listLastNotification());
                        sendNotification(getContext(), INTERNET_DOWN, 3);
                    }
                } else {
                    cancelJob();
                    mPulsator.start();
                    mPulsator.setCount(2);
                    connectionIcon.setImageResource(R.drawable.ic_satellite);
                    radarStatus.setTextColor(getResources().getColor(R.color.colorGray));
                    radarStatus.setText("INTERNET RADAR OFFLINE");
                    notificationManager.cancel(1);
                }
            }
        });

        menuIcon = (ImageView) findViewById(R.id.menuIcon);
        menuIcon.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                v.startAnimation(AnimationUtils.loadAnimation(context, R.anim.button));
                Intent mainIntent = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(mainIntent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);

        if (sharedPreferences != null){
            setTimerInterval(Integer.parseInt(sharedPreferences.getAll().get("polling_interval").toString()) * 1000);
        }

        mTimerReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent != null) {
                    Log.d(TAG, "Received Timer Broadcast into MainActivity!");
                    progressAnimator().cancel();
                    progressAnimator().start();
                }

            }
        };

        if (BuildConfig.PAID_VERSION) {// this is the flag configured in build.gradle
            //do nothing
        } else {
            MobileAds.initialize(this, "ca-app-pub-9180222631786193~8782422432");
            mAdView = (AdView) findViewById(R.id.adView);
            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);
        }


    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "ON START");
        mPulsator.stop();
        mPulsator.start();
        mPulsator.setCount(5);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "ON RESUME");

        IntentFilter deviceIdleIntent = assignDeviceIdleReceiver();
        registerReceiver(mDeviceIdleReceiver, deviceIdleIntent);

        IntentFilter filter = assignNetworkReceiver();
        registerReceiver(mNetworkReceiver, filter);

        //todo fix here when internet status is changed into background, ui not updated
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(internetStatusReceiver, new IntentFilter("internet.status.broadcast"));

        LocalBroadcastManager.getInstance(this)
                .registerReceiver(mTimerReceiver, new IntentFilter("timer-broadcast"));


        mPulsator.stop();

        if (isJobServiceOn(getContext())) {
            showCountdown();
            trigger.setChecked(true);
            mPulsator.setCount(5);
            mPulsator.setColor(INTERNET_DEFAULT.getNotificationColor());
            stopService(mServiceIntent);
            startService(mServiceIntent);
//            if (!isMyServiceRunning(mTimerService.getClass())) {
//                startService(mServiceIntent);
//            }
            if (databaseController.listLastNotification().equals("INTERNET AVAILABLE")) {
                connectionIcon.setImageResource(INTERNET_UP.getNotificationIcon());
                radarStatus.setTextColor(INTERNET_UP.getNotificationColor());
                radarStatus.setText(databaseController.listLastNotification());
                sendNotification(getContext(), INTERNET_UP, 1);
            } else if (databaseController.listLastNotification().equals("INTERNET NOT AVAILABLE")) {
                connectionIcon.setImageResource(R.drawable.ic_internet_connected_red);
                radarStatus.setTextColor(INTERNET_DOWN.getNotificationColor());
                radarStatus.setText(databaseController.listLastNotification());
                sendNotification(getContext(), INTERNET_DOWN, 3);
            }
        } else {
            hideCountdown();
            trigger.setChecked(false);
            mPulsator.setColor(INTERNET_DOWN.getNotificationColor());
            mPulsator.setCount(2);
        }

        mPulsator.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "ON PAUSE");
        mPulsator.stop();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        mPulsator.start();
        mPulsator.setCount(5);
        Log.d(TAG, "ON RESTART");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "ON STOP");
        mPulsator.setCount(0);
        mPulsator.stop();
//        unregisterReceiver(mDeviceIdleReceiver);
//        unregisterReceiver(mTimerService);
//        unregisterReceiver(mNetworkReceiver);  //todo check if this is not introducing a bug
        progressAnimator().pause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mTimerReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(internetStatusReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "ON DESTROY");
        stopService(mServiceIntent);
        Intent broadcastIntent = new Intent("timer.restart.sensor");
        sendBroadcast(broadcastIntent);
        sharedPreferences.unregisterOnSharedPreferenceChangeListener(this);
        Toast.makeText(getApplicationContext(),
                "Doppler will keep running into Background",
                Toast.LENGTH_LONG).show();
    }

    public void scheduleJob() {
        ComponentName componentName = new ComponentName(this, JobSchedulerService.class);
        JobInfo info = new JobInfo.Builder(PING_DNS_JOB_ID, componentName)
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                .setPersisted(true)
                .setPeriodic(60000)
                .setRequiresDeviceIdle(false)
                .build();

        JobScheduler scheduler = (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);
        int resultCode = scheduler.schedule(info);

        if (resultCode == JobScheduler.RESULT_SUCCESS) {
            Log.d(TAG, "Ping DNS Job scheduled");
        } else {
            Log.d(TAG, "Ping DNS Job scheduling failed");
        }
    }

    public void cancelJob() {
        JobScheduler scheduler = (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);
        scheduler.cancel(PING_DNS_JOB_ID);
        stopService(mServiceIntent);
        mPulsator.setCount(1);
        mPulsator.setColor(INTERNET_DOWN.getNotificationColor());
        hideCountdown();
        Log.d(TAG, "Job cancelled");
    }

    @NonNull
    private IntentFilter assignNetworkReceiver() {
        mNetworkReceiver = new NetworkChangeReceiver();

        IntentFilter filter = new IntentFilter();
        filter.addAction("service.to.activity.transfer");
        mNetworkReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent != null) {
                    String connectionType = intent.getStringExtra("connectionType");
                    Log.d(TAG, "onReceive: connectionType:" + connectionType);
                    getNetworkIcon(connectionType);
                }

            }
        };
        return filter;
    }

    @NonNull
    private IntentFilter assignDeviceIdleReceiver() {
        IntentFilter deviceIdleIntent = new IntentFilter();
        deviceIdleIntent.addAction(PowerManager.ACTION_DEVICE_IDLE_MODE_CHANGED);
        //todo use this for power saver notification, call PowerManager...
        //deviceIdleIntent.addAction(PowerManager.ACTION_POWER_SAVE_MODE_CHANGED);

        mDeviceIdleReceiver = new DeviceIdleReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent != null) {
                    Log.w(TAG, "[Device Idle Broadcast] Device idle changed state, restarting timer...");
//                    mServiceIntent = new Intent(getApplicationContext(), mTimerService.getClass());
                    stopService(mServiceIntent);
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            Log.i(TAG, "Timer Service started under Handler Delay statement");
                            startService(mServiceIntent);
                        }
                    }, 1000);   //1 second
                }
            }
        };

        return deviceIdleIntent;
    }

    private BroadcastReceiver internetStatusReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            NotificationType result = (NotificationType) intent.getSerializableExtra("internetStatus");
            Log.d(TAG, "INTERNET STATUS CHANGED TO:" + result.getNotificationTitle());
            if (result.equals(INTERNET_UP)) {
                connectionIcon.setImageResource(INTERNET_UP.getNotificationIcon());
                radarStatus.setTextColor(INTERNET_UP.getNotificationColor());
                radarStatus.setText("INTERNET AVAILABLE");

            } else {
                connectionIcon.setImageResource(R.drawable.ic_internet_connected_red);
                radarStatus.setTextColor(INTERNET_DOWN.getNotificationColor());
                radarStatus.setText("INTERNET NOT AVAILABLE");

            }

        }
    };

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isTimerServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isTimerServiceRunning?", false + "");
        return false;
    }

    private void getNetworkIcon(String connectionInfo) {

        switch (connectionInfo) {
            case "wifi-connection":
                networkIcon.setImageResource(R.drawable.ic_connection_white);
                connectionStatus.setText("WiFi Connection");
                break;
            case "data-connection":
                networkIcon.setImageResource(R.drawable.ic_signal);
                connectionStatus.setText("Mobile Data Connection");
                break;
            case "vpn-connection":
                networkIcon.setImageResource(R.drawable.ic_vpn);
                connectionStatus.setText("VPN Connection");
                break;
            default:
                networkIcon.setImageResource(R.drawable.ic_internet_connections_off);
                connectionStatus.setText("Network Disconnected");
                break;
        }
    }

    private ObjectAnimator progressAnimator() {

        ObjectAnimator progressAnimator = ObjectAnimator.ofInt(mProgressBar, "progress", 10000, 0);
        progressAnimator = ObjectAnimator.ofInt(mProgressBar, "progress", 10000, 0);

        if (android.os.Build.VERSION.SDK_INT == 27){
            progressAnimator.setDuration(getTimerInterval() * 2);
        } else{
            progressAnimator.setDuration(getTimerInterval());
        }

        progressAnimator.setRepeatCount(ObjectAnimator.INFINITE);
        progressAnimator.setRepeatMode(ObjectAnimator.RESTART);
        progressAnimator.setInterpolator(new LinearInterpolator());

        return progressAnimator;
    }

    private void hideCountdown() {
        mProgressBar.setVisibility(View.INVISIBLE);
        nextCheck.setVisibility(View.INVISIBLE);
    }

    private void showCountdown() {
        mProgressBar.setVisibility(View.VISIBLE);
        nextCheck.setVisibility(View.VISIBLE);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Log.d(TAG, "Shared Preferences Updated!");
        SharedPreferences preferenceManager = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        boolean isNotificationSticky = Boolean.parseBoolean(preferenceManager.getAll().get("notification_switch").toString());
        Log.d(TAG, String.format("Is Notification Sticky updated to: %s", isNotificationSticky));
        if (!isNotificationSticky) {
            notificationManager.cancel(1);
            notificationManager.cancel(3);
        }

        if (preferenceManager.getAll().get("polling_interval") != null) {
            int sharedPrefTimerInterval = Integer.parseInt(preferenceManager.getAll().get("polling_interval").toString()) * 1000;

            if (sharedPrefTimerInterval != getTimerInterval()) {
                setTimerInterval(sharedPrefTimerInterval);
                stopService(mServiceIntent);

                if (isJobServiceOn(getContext())){
                    startService(mServiceIntent);
                }

                Log.d(TAG, String.format("Timer Interval updated to %s milliseconds", getTimerInterval()));
                Toast.makeText(getApplicationContext(),
                        String.format("Polling Interval set to %s seconds", getTimerInterval() / 1000),
                        Toast.LENGTH_LONG).show();

            }

        } else {
            setTimerInterval(60000);
        }
    }

    private void checkAppFirstRun() {
        SharedPreferences settings = this.getSharedPreferences("appInfo", 0);
        boolean firstTime = settings.getBoolean(PREF_USER_FIRST_TIME, true);
        if (firstTime) {
            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean(PREF_USER_FIRST_TIME, false);
            editor.apply();
        }


        if (firstTime) {
            new FancyShowCaseView.Builder(this)
                    .delay(1000)
                    .focusCircleRadiusFactor(1.3)
                    .focusOn((SwitchCompat) findViewById(R.id.trigger))
                    .titleStyle(R.style.TutorialText, Gravity.TOP | Gravity.CENTER_HORIZONTAL)
                    .title("Start monitoring Internet Connection")
                    .backgroundColor(Color.parseColor("#029e84"))
                    .closeOnTouch(true)
                    .build()
                    .show();

        }
    }

}
