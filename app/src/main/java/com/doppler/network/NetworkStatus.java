package com.doppler.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by lucian on 08.09.2018.
 */

public class NetworkStatus {

    private static final String GOOGLE_DNS_SERVER = "8.8.8.8";
    private static final String CLOUDFLARE_DNS_SERVER = "1.1.1.1";
    private static final String TAG = "OUTGOING-NET-STATUS";
    private static final String RETRY_TAG = "DNS-CHECK";

    private String className;
    private Context context;

    public NetworkStatus() {
    }

    public NetworkStatus(Class className, Context context) {
        this.className = className.getSimpleName();
        this.context = context;
    }

    public boolean pingDnsServerSuccessful() {
        boolean success = false;
        int count = 0;
        final int MAX_TRIES = 5;

        while (!success && count++ < MAX_TRIES) {
            Log.d(RETRY_TAG, String.format("Retry value[%s]: %s out of %s", this.className, count, MAX_TRIES));
            success = isPingSuccessful(CLOUDFLARE_DNS_SERVER) || isPingSuccessful(GOOGLE_DNS_SERVER);
        }

        if (success) {
            Log.d(TAG, "Outgoing Internet Traffic is Enabled");
        } else {
            Log.d(TAG, "Error reaching Outgoing Traffic");
        }

        return success;

    }

    private boolean isDnsServerReachable(String dnsServer) {
        final int CONNECTION_TIMEOUT_MS = 250;

        try {
            return InetAddress.getByName(dnsServer).isReachable(CONNECTION_TIMEOUT_MS);
        } catch (Exception e) {
            Log.d(TAG, "Exception: Error while pinging " + dnsServer + " DNS server: " + e);
        }

        return false;
    }

    private boolean isPingSuccessful(String server) {

        String command = String.format("ping -c 1 -W 1 %s", server);

        try {
            return (Runtime.getRuntime().exec(command).waitFor() == 0);
        } catch (InterruptedException e) {
            Log.d(TAG, "Exception: Error while pinging: " + command);
            e.printStackTrace();
        } catch (IOException e) {
            Log.d(TAG, "Exception: Error while pinging " + command);
            e.printStackTrace();
        }

        return false;
    }

    public String getNetworkConnectionType(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo Info = cm.getActiveNetworkInfo();
        if (Info == null || !Info.isConnectedOrConnecting()) {
            Log.i(TAG, "No connection");
            return "no-connection";
        } else {
            int netType = Info.getType();
            int netSubtype = Info.getSubtype();

            if (isVpnConnection()) {
                return "vpn-connection";
            }

            if (netType == ConnectivityManager.TYPE_WIFI) {
                Log.i(TAG, "Wifi connection");
                return "wifi-connection";
            } else if (netType == ConnectivityManager.TYPE_MOBILE) {
                Log.i(TAG, "GPRS/3G connection");
                return "data-connection";
            }
        }

        return null;
    }

    private boolean isVpnConnection() {
        List<String> networkList = new ArrayList<>();
        try {
            for (NetworkInterface networkInterface : Collections.list(NetworkInterface.getNetworkInterfaces())) {
                if (networkInterface.isUp())
                    networkList.add(networkInterface.getName());
            }
        } catch (Exception ex) {
            Log.d(TAG, "isVpnUsing Network List didn't received");
        }

        return networkList.contains("tun0");
    }
}

