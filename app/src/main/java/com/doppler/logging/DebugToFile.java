package com.doppler.logging;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.IOException;

/**
 * Created by radu on 03.10.2018.
 */

public class DebugToFile {

    public static void debugWritingToSdCard(Context context) {
        if ( isExternalStorageWritable() ) {

            File appDirectory = new File( Environment.getExternalStorageDirectory() + "/MyPersonalAppFolder" );
            File logDirectory = new File( appDirectory + "/log" );
            File logFile = new File( logDirectory, "logcat" + System.currentTimeMillis() + ".txt" );


            Log.d("MainActivity", ">> Let's debug why this directory isn't being created: ");
            Log.d("MainActivity", "Is it working?: " + logDirectory.mkdirs());
            Log.d("MainActivity", "Does it exist?: " + logDirectory.exists());
            Log.d("MainActivity", "What is the full URI?: " + logDirectory.toURI());
            Log.d("MainActivity", "--");
            Log.d("MainActivity", "Can we write to this file?: " + logDirectory.canWrite());
            if (!logDirectory.canWrite()) {
                Log.d("MainActivity", ">> We can't write! Do we have WRITE_EXTERNAL_STORAGE permission?");
                if (context.checkCallingOrSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE") == PackageManager.PERMISSION_DENIED) {
                    Log.d("MainActivity", ">> We don't have permission to write - please add it.");
                } else {
                    Log.d("MainActivity", "We do have permission - the problem lies elsewhere.");
                }
            }
            Log.d("MainActivity", "Are we even allowed to read this file?: " + logDirectory.canRead());
            Log.d("MainActivity", "--");
            Log.d("MainActivity", ">> End of debugging.");

            // create app folder
            if ( !appDirectory.exists() ) {
                appDirectory.mkdir();
            }

            // create log folder
            if ( !logDirectory.exists() ) {
                logDirectory.mkdir();
            }

            // clear the previous logcat and then write the new one to the file
            try {
                Process process = Runtime.getRuntime().exec("logcat -c");
                process = Runtime.getRuntime().exec("logcat -f " + logFile);
            } catch ( IOException e ) {
                e.printStackTrace();
            }

        } else if ( isExternalStorageReadable() ) {
            // only readable
        } else {
            // not accessible
        }
    }

    /* Checks if external storage is available for read and write */
    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    /* Checks if external storage is available to at least read */
    public static boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state);
    }
}
