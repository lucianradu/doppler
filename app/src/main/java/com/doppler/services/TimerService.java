package com.doppler.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.doppler.database.DatabaseController;
import com.doppler.network.NetworkStatus;
import com.doppler.notification.NotificationManager;
import com.doppler.notification.NotificationType;

import java.util.Timer;
import java.util.TimerTask;

import static android.content.ContentValues.TAG;
import static com.doppler.utils.PowerSaverHelper.isDeviceIntoDozeMode;

public class TimerService extends Service {

    public int counter = 0;
    private Timer timer;
    private TimerTask timerTask;
    private static final String PING_TAG = "PING-DNS-SERVER";
    private static final String NETWORK_TAG = "NETWORK-CHECK";
    private static final String NOTIFICATION_TAG = "NOTIFICATION";
    private NetworkStatus networkStatus;
    private NotificationManager notificationManager;
    private DatabaseController databaseController;

    public TimerService(Context applicationContext) {
        super();
        Log.i("TIMER", "here I am!");
    }

    public TimerService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        startTimer();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("EXIT Timer", "ondestroy!");
        stoptimertask();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void startTimer() {
        Log.i(PING_TAG, "Starting Timer Service");
        //set a new Timer
        timer = new Timer();

        //TODO create instance before startTimer
        networkStatus = new NetworkStatus(TimerService.class, getApplicationContext());
        notificationManager = new NotificationManager();
        databaseController = new DatabaseController(this);

        int timerInterval = 60; //seconds

        //initialize the TimerTask's job
        initializeTimerTask();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        if (preferences.getAll().get("polling_interval") != null) {
            timerInterval = Integer.parseInt(preferences.getAll().get("polling_interval").toString()) * 1000;
        } else {
            timerInterval = timerInterval * 1000;
        }

        //schedule the timer, to wake up every 5 minutes
        timer.schedule(timerTask, 0, timerInterval); //
    }

    /**
     * it sets the timer to print the counter every x seconds
     */
    public void initializeTimerTask() {

        timerTask = new TimerTask() {
            public void run() {

                Log.i("in timer", "in timer ++++  " + (counter++));
                Intent intent = new Intent("timer-broadcast");
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

                ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo netInfo = cm.getActiveNetworkInfo();
                boolean hasNetworkAccess = netInfo != null && netInfo.isConnectedOrConnecting();
                boolean isNetworkAvailable = netInfo != null && netInfo.isAvailable();

                String networkStats = "Network connected: " + hasNetworkAccess + " Network available: " + isNetworkAvailable;
                Log.i(TAG, networkStats);

                if (!isDeviceIntoDozeMode(getApplicationContext()) && hasNetworkAccess) {
                    Log.d(TAG, "[Timer] Doze Mode Disabled - Perform PING using Timer");

                    if (networkStatus.pingDnsServerSuccessful()) {
                        Log.i(PING_TAG, "Active Internet Connection");
                        if (!isDeviceIntoDozeMode(getApplicationContext())
                                && !databaseController.listLastNotification().equals("INTERNET AVAILABLE")) {
                            Log.i(NOTIFICATION_TAG, "Internet ON");
                            notificationManager.sendNotification(getApplicationContext(), NotificationType.INTERNET_UP, 1);

                            try {
                                databaseController.insertNotificationStatus("INTERNET AVAILABLE");
                            } catch (SQLiteException e) {
                                Log.e(TAG, "Error while inserting into database", e);
                            }

                        }


                    } else {
                        Log.e(PING_TAG, "No Internet Connection");
                        if (!isDeviceIntoDozeMode(getApplicationContext())
                                && !databaseController.listLastNotification().equals("INTERNET NOT AVAILABLE")) {
                            Log.e(NOTIFICATION_TAG, "Internet OFF");
                            notificationManager.sendNotification(getApplicationContext(), NotificationType.INTERNET_DOWN, 3);

                            try {
                                databaseController.insertNotificationStatus("INTERNET NOT AVAILABLE");
                            } catch (SQLiteException e) {
                                Log.e(TAG, "Error while inserting into database", e);
                            }

                        }


                    }
                } else if (!isDeviceIntoDozeMode(getApplicationContext()) && !isNetworkAvailable) {

                    Log.e(PING_TAG, "Network Disconnected");
                    if (!isDeviceIntoDozeMode(getApplicationContext())
                            && !databaseController.listLastNotification().equals("INTERNET NOT AVAILABLE")) {
                        Log.e(NOTIFICATION_TAG, "Internet OFF");
                        notificationManager.sendNotification(getApplicationContext(), NotificationType.INTERNET_DOWN, 3);

                        try {
                            databaseController.insertNotificationStatus("INTERNET NOT AVAILABLE");
                        } catch (SQLiteException e) {
                            Log.e(TAG, "Error while inserting into database", e);
                        }

                    }
                } else {
                    Log.w(TAG, "[Timer] Doze Mode Enabled - Waiting for Doze Maintenance Window");
//                    stoptimertask();
                }



            }
        };
    }

    /**
     * not needed
     */
    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

}