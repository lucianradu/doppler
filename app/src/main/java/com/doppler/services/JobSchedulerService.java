package com.doppler.services;

import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.doppler.database.DatabaseController;
import com.doppler.logging.PermanentLoggerUtil;
import com.doppler.network.NetworkStatus;
import com.doppler.notification.NotificationManager;
import com.doppler.notification.NotificationType;

import static com.doppler.utils.PowerSaverHelper.isDeviceIntoDozeMode;

public class JobSchedulerService extends JobService {

    public static final int PING_DNS_JOB_ID = 1;
    private boolean jobCancelled = false;
    private Intent mServiceIntent;
    private static final String TAG = "PING_DNS_JOB";

    @Override
    public boolean onStartJob(JobParameters params) {
        if (params.getJobId() == PING_DNS_JOB_ID) {
            Log.d(TAG, "Timer Service stopped by JobScheduler, preparing to do background work..");
            TimerService mTimerService = new TimerService(getApplicationContext());
            mServiceIntent = new Intent(getApplicationContext(), mTimerService.getClass());

            stopService(mServiceIntent);
            doBackgroundWork(params);
        }

        return true;
    }

    private void doBackgroundWork(final JobParameters params) {

        if (jobCancelled) {
            return;
        }

        Log.d(TAG, "Background work in progress, JobScheduler is starting Timer..");
        startService(mServiceIntent);
        jobFinished(params, false);

    }


    @Override
    public boolean onStopJob(JobParameters params) {
        Log.d(TAG, "Job cancelled before completion");
        jobCancelled = true;
        return true;
    }

    public static boolean isJobServiceOn( Context context ) {
        JobScheduler scheduler = (JobScheduler) context.getSystemService( Context.JOB_SCHEDULER_SERVICE ) ;

        boolean hasBeenScheduled = false ;

        for ( JobInfo jobInfo : scheduler.getAllPendingJobs() ) {
            if ( jobInfo.getId() == PING_DNS_JOB_ID ) {
                hasBeenScheduled = true ;
                break ;
            }
        }

        return hasBeenScheduled ;
    }

}