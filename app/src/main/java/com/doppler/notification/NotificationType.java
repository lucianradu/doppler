package com.doppler.notification;

import android.graphics.Color;

import com.doppler.R;

/**
 * Created by lucian on 09.09.2018.
 */

public enum NotificationType {

    INTERNET_DOWN("Connection Status - Offline", "Internet Connection Not Available", Color.rgb(188,86,81), R.drawable.ic_internet_connections_off),
    INTERNET_UP("Connection Status - Online", "Internet Connection Available", Color.rgb(2,158,132), R.drawable.ic_internet_connected),
    INTERNET_DEFAULT("Default Status", "Please ignore", Color.rgb(2, 158, 132), R.drawable.ic_wifi);

    private String notificationTitle;
    private String notificationContent;
    private int notificationColor;

    private int notificationIcon;

    NotificationType(String notificationTitle, String notificationContent, int notificationColor, int notificationIcon) {
        this.notificationTitle = notificationTitle;
        this.notificationContent = notificationContent;
        this.notificationColor = notificationColor;
        this.notificationIcon = notificationIcon;
    }

    public String getNotificationTitle() {
        return notificationTitle;
    }

    public String getNotificationContent() {
        return notificationContent;
    }

    public int getNotificationColor() {
        return notificationColor;
    }

    public int getNotificationIcon() {
        return notificationIcon;
    }

}
